import React from 'react'
import ReactDOM from 'react-dom'
import Board from './Board'
import './style.scss'


ReactDOM.render(<Board />, document.getElementById('app'))