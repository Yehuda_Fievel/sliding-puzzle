import React, { useState, useEffect } from 'react'


const Board = () => {
    const [numbers, setNumbers] = useState([1, 2, 0, 4, 5, 6, 3, 8, 9, 10, 7, 12, 13, 14, 11, 15])

    useEffect(() => {
        if (isSorted([...numbers])) {
            alert('You won the game')
        }
    }, [numbers])

    const onClick = (index) => {
        const zero = numbers.findIndex(n => n == 0)

        if (index + 1 == zero || index - 1 == zero || index - 4 == zero || index + 4 == zero) {
            [numbers[zero], numbers[index]] = [numbers[index], numbers[zero]]
            setNumbers([...numbers])
        }
    }

    const isSorted = array => {
        array.pop()
        return array.every((num, i, arr) => (num <= arr[i + 1]) || (i === arr.length - 1) ? 1 : 0);
    }

    return (
        <div className='container'>
            {numbers.map((num, i) => (
                <div
                    key={i}
                    className={`box ${num == 0 ? 'zero' : ''}`}
                    onClick={() => onClick(i)}
                >
                    {num != 0 ? num : ''}
                </div>
            ))}
        </div>
    )
}


export default Board