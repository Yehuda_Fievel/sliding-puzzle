const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin')


module.exports = env => {
    return {
        devtool: 'inline-source-map',
        entry: path.resolve(__dirname, 'index.js'),
        output: {
            path: path.resolve(__dirname, 'dist'),
            publicPath: '/',
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                '@babel/preset-env',
                                '@babel/preset-react',
                            ],
                        },
                    },
                },
                {
                    test: /\.html$/,
                    use: [{ loader: 'html-loader' }],
                },
                {
                    test: /\.s[ac]ss$/i,
                    use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'sass-loader' }],
                },
            ],
        },
        plugins: [
            new HtmlWebPackPlugin({ template: './index.html', filename: './index.html' }),
        ],
    }
}